import React, { useState, Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
	state = {
		person: [
			{ name: 'akhil', age: 20 },
			{ name: 'ram', age: 22 },
			{ name: 'raj', age: 24 },
		]
	}

	switchNameHandler = (newName) => {
		console.log('-------------------------------------[1]');
		this.setState({
			person: [
				{ name: newName, age: 20 },
				{ name: 'ram', age: 22 },
				{ name: 'raj', age: 26 },
			]
		})
	}
	changeNameHandler = (event)=>{
		this.setState({
			person: [
				{ name: 'max', age: 20 },
				{ name: event.target.value, age: 22 },
				{ name: 'raj', age: 26 },
			]
		})
	}
	render() {
		const style={
			backgroundColor: 'red',
			padding:'10px'
		}
		return(
			<div className="app-div">
				<button style={style} onClick={this.switchNameHandler.bind(this,'rakesh')}>
					Switch name
			</button>
				<Person
					name={this.state.person[0].name}
					age={this.state.person[0].age} />
				<Person
					name={this.state.person[1].name}
					age={this.state.person[1].age} 
					click={this.switchNameHandler.bind(this, 'ramu')} 
					changed={this.changeNameHandler}
					>My hobies</Person>
				<Person
					name={this.state.person[2].name}
					age={this.state.person[2].age} />
			</div>
		)
	}
}
export default App;