import React  from 'react';
// let a =  Math.floor(Math.random() * 30);
import  './Person.css'
const Person = (props)=>{
    return <div className="Person">
        <h1 onClick={props.click}>Hi {props.name}, my age is {props.age}. {props.children} </h1>
        <input onChange ={props.changed}></input>
    </div>;
}
export default Person;